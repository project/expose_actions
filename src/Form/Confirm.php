<?php

namespace Drupal\expose_actions\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\system\Entity\Action;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class Confirm extends ConfirmFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The action entity.
   *
   * @var \Drupal\system\Entity\Action|null
   */
  protected ?Action $action;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  protected ?EntityInterface $entity;

  /**
   * Confirm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    try {
      $this->loadAction($this->getRequest()->attributes->get('action'));
      $this->loadEntity($this->getRequest()->attributes->get('entity_type'), $this->getRequest()->attributes->get('entity_id'));
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      // @todo Handle exception.
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Confirm {
    return new Confirm(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Loads the action entity.
   *
   * @param string|null $action_id
   *   The action ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadAction(?string $action_id = NULL): void {
    if ($action_id !== NULL && !isset($this->action)) {
      $this->action = $this->entityTypeManager->getStorage('action')
        ->load($action_id);
    }
  }

  /**
   * Loads the entity.
   *
   * @param string|null $entity_type
   *   The entity type.
   * @param string|null $entity_id
   *   The entity ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEntity(?string $entity_type = NULL, ?string $entity_id = NULL): void {
    if ($entity_type !== NULL && $entity_id !== NULL && !isset($this->entity)) {
      $this->entity = $this->entityTypeManager->getStorage($entity_type)
        ->load($entity_id);
    }
  }

  /**
   * Check whether the user has 'administer' or 'overview' permission.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param string|null $action_id
   *   The action ID.
   * @param string|null $entity_type
   *   The entity type.
   * @param string|null $entity_id
   *   The entity ID.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function checkAccess(AccountInterface $account, ?string $action_id = NULL, ?string $entity_type = NULL, ?string $entity_id = NULL): AccessResultInterface {
    $this->loadAction($action_id);
    if (!isset($this->action) || !$account->hasPermission('access exposed action ' . $this->action->id())) {
      return AccessResult::forbidden();
    }
    $this->loadEntity($entity_type, $entity_id);
    return $this->entity->access('view', $account, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'confirm_expose_action';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    return $this->t('You are about to execute %action on @type %entity. This can not be undone.', [
      '%action' => $this->action->label(),
      '@type' => $this->entity->getEntityType()->getLabel(),
      '%entity' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getCancelUrl(): Url {
    return $this->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->action->execute([$this->entity]);
    $this->messenger()->addStatus($this->t('%action completed!', [
      '%action' => $this->action->label(),
    ]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
