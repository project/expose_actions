<?php

namespace Drupal\expose_actions;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides permission callback for local actions.
 *
 * @package Drupal\expose_action
 */
class Permissions {

  use StringTranslationTrait;

  /**
   * Helper function to provide permissions for local actions.
   *
   * @return array
   *   The permissions.
   */
  public function generate(): array {
    $permissions = [];
    try {
      /** @var \Drupal\system\Entity\Action $action */
      // @phpstan-ignore-next-line
      foreach (\Drupal::entityTypeManager()->getStorage('action')->loadMultiple() as $id => $action) {
        $permissions['access exposed action ' . $id] = [
          'title' => $this->t('Use the exposed action <a href="@url">@label</a>', [
            '@url' => $action->toUrl()->toString(),
            '@label' => $action->label(),
          ]),
        ];
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityMalformedException) {
      // Ignore these exceptions.
    }
    return $permissions;
  }

}
