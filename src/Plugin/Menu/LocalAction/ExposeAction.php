<?php

namespace Drupal\expose_actions\Plugin\Menu\LocalAction;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides local action menu plugin.
 */
class ExposeAction extends LocalActionDefault {

  /**
   * Sets the route parameters for the local actions.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return array
   *   The route parameters.
   */
  public function getRouteParameters(RouteMatchInterface $route_match): array {
    $parameters = parent::getRouteParameters($route_match);
    foreach ($route_match->getParameters()->all() as $entity) {
      if ($entity instanceof EntityInterface) {
        $parameters['entity_type'] = $entity->getEntityTypeId();
        $parameters['entity_id'] = $entity->id();
      }
    }
    return $parameters;
  }

}
